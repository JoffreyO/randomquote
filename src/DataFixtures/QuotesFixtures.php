<?php

namespace App\DataFixtures;

use App\Entity\Quotes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class QuotesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
         $quote1 = new Quotes();
         $quote1->setName("Le commencement est la moitié de tout");
         $quote1->setAuthor("Pythagore");

         $quote2 = new Quotes();
         $quote2->setName("La vie est comme une bicyclette pour garder l'équilibre il faut avancer");
         $quote2->setAuthor("Albert Einstein");

         $quote3 = new Quotes();
         $quote3->setName("Devenir proactif, c’est agir, plutôt que réagir");
         $quote3->setAuthor("Mylène Muller");

         $quote4 = new Quotes();
         $quote4->setName("Si vous ne pouvez pas battre la machine, le mieux est d'en devenir une");
         $quote4->setAuthor("Elon Musk");

         $quote5 = new Quotes();
         $quote5->setName("Je pense que c’est le meilleur conseil : réfléchissez constamment à la façon de mieux faire les choses et de vous poser des questions.");
         $quote5->setAuthor("Elon Musk");

         $quote6 = new Quotes();
         $quote6->setName("Dieu ne joue pas aux dés");
         $quote6->setAuthor("Albert Einstein");

         $quote7 = new Quotes();
         $quote7->setName("La vie est un mystère qu'il faut vivre, et non un problème à résoudre");
         $quote7->setAuthor("Gandhi");

         $quote8 = new Quotes();
         $quote8->setName("Je ne cherche pas à connaître les réponses, je cherche à comprendre les questions");
         $quote8->setAuthor("Confucius");

         $quote9 = new Quotes();
         $quote9->setName("Choisissez un travail que vous aimez et vous n'aurez pas à travailler un seul jour de votre vie");
         $quote9->setAuthor("Confucius");

         $quote10 = new Quotes();
         $quote10->setName("Le soupçon détruit la confiance niveau par niveau");
         $quote10->setAuthor("Thomas H.Cook");

         $manager->persist($quote1);
         $manager->persist($quote2);
         $manager->persist($quote3);
         $manager->persist($quote4);
         $manager->persist($quote5);
         $manager->persist($quote6);
         $manager->persist($quote7);
         $manager->persist($quote8);
         $manager->persist($quote9);
         $manager->persist($quote10);

        $manager->flush();
    }
}
