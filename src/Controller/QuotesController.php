<?php

namespace App\Controller;

use App\Entity\Quotes;
use App\Repository\QuotesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuotesController extends AbstractController
{
    private $repoQuotes;

    public function __construct(QuotesRepository $repoQuotes){
        $this->repoQuotes = $repoQuotes;
    }

    #[Route('/', name: 'app_quotes')]
    public function index(): Response
    {
        $quote = $this->getRandomQuote();
        
        return $this->render('quotes/index.html.twig', [
            'controller_name' => 'QuotesController',
            'quote'=> $quote
        ]);
    }

    public function getRandomQuote(): Quotes{
        //On récupére le nombre de citations en bdd
        $nbRows = $this->repoQuotes->countTotalRows();

        //On fait un random pour choisir la citation
        $rand = rand(1, $nbRows);
        $quoteSelected = $this->repoQuotes->find($rand);
        return $quoteSelected;
    }
}
