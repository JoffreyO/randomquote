const hex = [
            '#7EBBD4', 
            '#AA39A5', 
            '#CDD582', 
            '#A87538', 
            '#E3AAB7', 
            '#222765',
            '#F2DAD7',
            '#020101',
            '#329754',
            '#3DB4B7',
            '#8287D5',
            '#BC5A43'
        ];

randomColor();

function randomColor(){
    //On récupérer la taille du tableau
    let max = hex.length;

    //On fait un random pour choisir la couleur
    let random = Math.floor(Math.random() * (max - 0 ));

    //On change la couleur du background de manière random
    document.body.style.backgroundColor = hex[random];
}

function reload(){
    location.reload();
}